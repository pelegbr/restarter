package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"./Libs/DbCon"
	. "./Libs/Helper"
	"./Libs/Logger"
	"github.com/json-iterator/go"
)

type (
	SqlConnection struct {
		SqlDriver      string
		SqlConString   string
		SqlConId       string
		PointerTo      string
		IsDaily        bool
		MustConnect    bool
		AttachedDbs    []DbCon.AttachDb
		BackUpFileName string
		LockStyle      int64
	}

	Response struct {
		Cmd string `json:"cmd"`
	}

	Configuration struct {
		ListenOnPort   string
		SqlConnections []SqlConnection
	}
)

var AppConf Configuration

func ReadConf() error {
	_, fileName := Logger.GetCurrentDir()
	fileName += "/MyConf.conf"
	file, fileErr := os.OpenFile(fileName, os.O_RDWR, 0)
	defer file.Close()
	if fileErr != nil {
		return fmt.Errorf("ReadConf err: Error while openning a configuration file: %s", fileErr.Error())
	} else {
		decoder := jsoniter.NewDecoder(file)
		confErr := decoder.Decode(&AppConf)
		if confErr != nil {
			Logger.PrintToLog("ReadConf err: " + confErr.Error())
			return confErr
		}
	}
	return nil
}

func main() {
	Logger.PrintToLog("Starting process at ", time.Now().String())
	if err := ReadConf(); err != nil {
		Logger.PrintToLog("Error reading Conf ", err.Error())
		return
	}

	//initializing DB connections
	if err := InitDbConConnections(); err != nil {
		Logger.PrintToLog("main err: ", err.Error())
		return
	}

	Logger.PrintToLog("Listening On Port: ", AppConf.ListenOnPort)
	http.HandleFunc("/getOP/", handle)
	http.ListenAndServe(":"+AppConf.ListenOnPort, nil)
}

func handle(response http.ResponseWriter, request *http.Request) {
	defer request.Body.Close()
	Logger.PrintToLog("got a http request from:", request.URL)
	stid := request.URL.Path
	if stidIndex := strings.LastIndex(stid, "/"); stidIndex == -1 {
		fmt.Print(request.URL.Path)
		response.Write([]byte("No stationId provided."))
		return
	} else {
		stid = string(stid[stidIndex+1:])
	}
	query := `Select StationCmd from cmd where StationId = ? limit 1`
	if sharedId, err := DbCon.GetDbValueStr("restarter", query, stid); err == nil {
		if sharedId != "" {
			if bytes, err := mkres(sharedId); err == nil {
				fmt.Print(string(bytes))
				response.Write(bytes)
				Logger.PrintToLog("response: " + string(bytes))
			} else {
				fmt.Errorf("Couldn't respond back to %s err: %s", request.URL, err.Error())
			}
			return
		} else {
			fmt.Errorf("StationCmd of %s is null", request.URL, err)
		}
	} else {
		response.Write([]byte("wrong station Id"))
		Logger.PrintToLog("an error occured: ", err.Error())
	}
}

func mkres(val string) ([]byte, error) {
	response := Response{val}
	fmt.Println(response)
	return jsoniter.Marshal(response)
}

func InitDbConConnections() error {

	for i := range AppConf.SqlConnections {

		if !AppConf.SqlConnections[i].IsDaily && strings.Contains(AppConf.SqlConnections[i].SqlConString, "$token:date$") {
			AppConf.SqlConnections[i].IsDaily = true
			if AppConf.SqlConnections[i].SqlConId == DbConIdSqlIoLog {
				_, currentDir := Logger.GetCurrentDir()
				source := currentDir + "/" + IoLogDbFile

				if _, err := os.Stat(source); os.IsNotExist(err) {
					return fmt.Errorf("InitDbConConnections err: " + err.Error())
				}
			}
		}
		DbCon.InitConnection(AppConf.SqlConnections[i].SqlConId, AppConf.SqlConnections[i].SqlDriver,
			AppConf.SqlConnections[i].SqlConString, AppConf.SqlConnections[i].PointerTo,
			AppConf.SqlConnections[i].AttachedDbs, AppConf.SqlConnections[i].IsDaily,
			AppConf.SqlConnections[i].BackUpFileName, AppConf.SqlConnections[i].LockStyle)

	}
	return nil
}
